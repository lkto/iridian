import { Component } from '@angular/core';
import events from './../assets/data.json';
import {
  EventType,
  MarketType,
  SelectionType,
  OptionSelectedType,
} from './Entity/MarkEntity';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'bets';
  eventsData = <EventType[]>events;
  optionSelected: OptionSelectedType[] = [];

  selectOption(event: EventType, market: MarketType, selection: SelectionType) {
    let tempOption: OptionSelectedType = {
      idEvent: event.id,
      nameEvent: event.name,
      idMarket: market.id,
      nameNarket: market.name,
      idOption: selection.id,
      nameOption: selection.name,
      price: selection.price,
    };

    this.addToSelected(tempOption);
  }

  addToSelected(option: OptionSelectedType) {
    let exist = false;
    this.optionSelected.map((optionSelec, key) => {
      if (
        optionSelec.idEvent === option.idEvent &&
        optionSelec.idMarket === option.idMarket
      ) {
        if (optionSelec.idOption === option.idOption) {
          exist = true;
        }
        this.optionSelected.splice(key);
      }
    });

    if (!exist) {
      this.optionSelected.push(option);
    }
  }

  isOptionSelected(idEvent: string, idMarket: string, idOption: string) {
    let checked = false;

    this.optionSelected.map((optionSelec, key) => {
      if (
        optionSelec.idEvent === idEvent &&
        optionSelec.idMarket === idMarket &&
        optionSelec.idOption === idOption
      ) {
        checked = true;
      }
    });

    return checked ? 'selectedOption' : '';
  }

  removeToSelected(selection: OptionSelectedType) {
    let objectkey = 0;
    this.optionSelected.map((optionSelec, key) => {
      if (
        optionSelec.idEvent === selection.idEvent &&
        optionSelec.idMarket === selection.idMarket &&
        optionSelec.idOption === selection.idOption
      ) {
        objectkey = key;
      }
    });

    this.optionSelected.splice(objectkey, 1);
  }
}
