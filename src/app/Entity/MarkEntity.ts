export interface EventType {
  id: string;
  name: string;
  markets: MarketType[];
}

export interface MarketType {
  id: string;
  name: string;
  selections: SelectionType[];
}

export interface SelectionType {
  id: string;
  name: string;
  price: number;
}

export interface OptionSelectedType {
  idEvent: string;
  nameEvent: string;
  idMarket: string;
  nameNarket: string;
  idOption: string;
  nameOption: string;
  price: number;
}
