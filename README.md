# Prueba unitaria desarrollador frondednd Iridian

Welcome to Iridian Frontend Tech test.
The primary goal of this exercise is to assess how you reason about your ability to write clean, well tested
and reusable code. There's no hard rules or tricky questions.

# Bets

Este proyecto fue realizado bajo las siguientes tecnologias:

    *** Angular CLI: 12.2.13
    *** Node : 14.18.0
    *** NPM : 6.14.15
    *** Angular: 12.2.13

# Despliegue

Para ejecutarlo primero asegurese tener todas las herramientas de desarrollo mencionadas anteriormente en las versiones alli estipuladas o superiores en su defecto.

    *** git clone https://gitlab.com/lkto/iridian.git
    *** nmp install
    *** ng serve


# Explicasion

Este proyecto responde a la prueba tecnica enviada por la empresa Iridian para el cargo de desarrollador fronend, para su desarrollo se eligio Angular ya que es el framework frond de Javascript preferido por la empresa.

Se utilizo el esquema de datos otorgado por la compañia para el desarrollo de la misma en el archivo json anexo a la prueba y se realizo el desarrollo con base en este.

Esta prueba no posee Test Unitarios debido a dos cosas:

    *** La compañia no lo estipulo como requerimiento obligatorio
    *** El framework recomendado paar ello no es de mi conocimiento